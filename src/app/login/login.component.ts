import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formGroup: FormGroup;
  isOptional = false;
  constructor(private _formBuilder: FormBuilder,private router:Router) { }

  ngOnInit(): void {
    this.formGroup = this._formBuilder.group({
      
      email: [null, [Validators.required, Validators.email]],
      password: [null, Validators.required]
      
    });
  }

}
