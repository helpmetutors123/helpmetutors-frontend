import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-teacherform',
  templateUrl: './teacherform.component.html',
  styleUrls: ['./teacherform.component.scss']
})
export class TeacherformComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  isOptional = false;


  constructor(private _formBuilder: FormBuilder,private router:Router) { }
  options ={
    componentRestrictions: {
      country : ['IND', 'CA','USA']
    }
  }


  ngOnInit(): void {
    this.firstFormGroup = this._formBuilder.group({
      // firstCtrl: ['', Validators.required],
      name: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
      password: [null, Validators.required],
      contact: [null,Validators.required],
      location: [null, Validators.required]
      

    });
    this.secondFormGroup = this._formBuilder.group({
      // secondCtrl: ['', Validators.required]
      about: [null, Validators.required],
      teaching: [null, Validators.required],
      work: [null, Validators.required],
      modeofteaching: [null,Validators.required,],
      
    });
    this.thirdFormGroup = this._formBuilder.group({
      
      timing: [null, Validators.required],
      charge: [null, Validators.required],
      upload: [null, Validators.required],
    });
  }
  onSubmitClick(){
   
    const data1={
     
    }
    console.log(data1);
    localStorage.setItem('data1', JSON.stringify(data1));
    this.router.navigate(['/thankyou']);
  
  }

}
