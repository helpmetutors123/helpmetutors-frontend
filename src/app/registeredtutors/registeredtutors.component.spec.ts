import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisteredtutorsComponent } from './registeredtutors.component';

describe('RegisteredtutorsComponent', () => {
  let component: RegisteredtutorsComponent;
  let fixture: ComponentFixture<RegisteredtutorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisteredtutorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisteredtutorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
