import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { event } from 'jquery';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Helpmetutors';
  showMenu:boolean=false;
  constructor(
    private router: Router
  ) {
       //ignore thankyou component
       router.events.forEach((event) =>{
         if(event instanceof NavigationStart){
           this.showMenu = event.url !=="/thankyou"
         }
       });
  }
  ngOnInit(){}
}
