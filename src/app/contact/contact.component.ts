import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import {MatDialog} from '@angular/material/dialog';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  
  formGroup: FormGroup;
  isOptional = false;
  submitted = false;
  constructor(private _formBuilder: FormBuilder,private router:Router,public dialog: MatDialog) { }

  ngOnInit(): void {
    this.formGroup = this._formBuilder.group({
      
      name: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
      phone: [null, Validators.required],
      subject: [null, Validators.required],
      message : [null, Validators.required],
      
    });
  }
  onSubmitClick(){
    console.log(this.formGroup.value)
   const data1={
     "name":this.formGroup.value.name,
     "email":this.formGroup.value.email,
     "phone":this.formGroup.value.phone,
     "subject":this.formGroup.value.subject,
     "message":this.formGroup.value.message
   }
   //console.log(data1);
   localStorage.setItem('data1', JSON.stringify(data1));
   //this.router.navigate(['/thankyou']);
  
  }

  
    // convenience getter for easy access to form fields
    get f() { return this.formGroup.controls; }


  onSubmit() {
    // if (this.formGroup.valid) {
    //   console.log("Form Submitted!");
    // }
    //
    // this.submitted = true;

    //     // stop here if form is invalid
    //     if (this.formGroup.invalid) {
    //         return;
    //     }

    //     alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.formGroup.value))

    this.dialog.open(ThankyouDialog);
    
  //   this.dialog.afterClosed().subscribe(data=>{
  //     console.log("data returned from mat-dialog-close is ",data);
  // })
  }
  
}

@Component({
  selector: 'thankyou-dialog',
  templateUrl: 'thankyou-dialog.html',
})
export class ThankyouDialog {}
