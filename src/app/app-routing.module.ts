import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { ContactComponent } from './contact/contact.component';
import { TestComponent } from './test/test.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ParentformComponent } from './parentform/parentform.component';
import { TeacherformComponent } from './teacherform/teacherform.component';
import { AboutComponent } from './about/about.component';
import { FooterComponent } from './footer/footer.component';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { ParentdashboardComponent } from './parentdashboard/parentdashboard.component';
import { FaqsComponent } from './faqs/faqs.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { TermsandconditionsComponent } from './termsandconditions/termsandconditions.component';
import { DisclaimerComponent } from './disclaimer/disclaimer.component';

const routes: Routes = [
  { path: 'header', component:HeaderComponent},
  { path: 'footer', component:FooterComponent},
  { path: 'contact', component:ContactComponent},
  { path: 'test', component:TestComponent},
  { path: 'login', component:LoginComponent},
  { path: 'register', component:RegisterComponent},
  { path: '', component:HomeComponent},
  { path: 'about', component:AboutComponent},
  { path: 'parentregistrationform', component:ParentformComponent},
  { path: 'teacherregistrationform', component:TeacherformComponent},
  { path: 'thankyou', component:ThankyouComponent},
  {path: 'parentdashboard', component:ParentdashboardComponent},
  { path: 'faqs', component:FaqsComponent},
  { path: 'privacypolicy', component:PrivacypolicyComponent},
  { path: 'termsandconditions', component:TermsandconditionsComponent},
  { path: 'disclaimer', component:DisclaimerComponent}
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
