import { Component, OnInit,  AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
declare let $: any;


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {



  constructor( private router:Router) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(){
    $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})
  }



  onButtonParentClick(){
   
    const data1={
     
    }
    console.log(data1);
    localStorage.setItem('data1', JSON.stringify(data1));
    this.router.navigate(['/parentregistrationform']);
  
  }
  onButtonTeacherClick(){
   
    const data1={
     
    }
    console.log(data1);
    localStorage.setItem('data1', JSON.stringify(data1));
    this.router.navigate(['/teacherregistrationform']);
  
  }
  onButtonContactClick(){
   
    const data1={
     
    }
    console.log(data1);
    localStorage.setItem('data1', JSON.stringify(data1));
    this.router.navigate(['/contact']);
  
  }
}
