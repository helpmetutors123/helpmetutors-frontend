import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-parentform',
  templateUrl: './parentform.component.html',
  styleUrls: ['./parentform.component.scss']
})
export class ParentformComponent implements OnInit {
 
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  isOptional = false;
  submitted = false;

  constructor(private _formBuilder: FormBuilder,private router:Router) { }

  options ={
    componentRestrictions: {
      country : ['IND', 'CA','USA']
    }
  }

  ngOnInit(): void {
    this.firstFormGroup = this._formBuilder.group({
      
      name: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
      password: [null, Validators.required],
      contact: [null, Validators.required],
      location : [null, Validators.required],
      
    });
    this.secondFormGroup = this._formBuilder.group({
     
      lookingfor: [null, Validators.required],
      grade: [null, Validators.required],
      subjects: [null, Validators.required],
      details: [null, Validators.required],
      
    });
    this.thirdFormGroup = this._formBuilder.group({
      
      modeofteaching: [null, Validators.required],
      gender: [null, Validators.required],
      budget: [null, Validators.required],
      budget1: [null, Validators.required],
      document: []
    });
  }

  onSubmitClick(){
    console.log(this.firstFormGroup.value),
    console.log(this.secondFormGroup.value),
    console.log(this.thirdFormGroup.value)
    const data1={
      "name":this.firstFormGroup.value.name,
      "email":this.firstFormGroup.value.email,
      "password":this.firstFormGroup.value.password,
      "contact":this.firstFormGroup.value.contact,
      "location":this.firstFormGroup.value.location,
      "lookingfor":this.secondFormGroup.value.lookingfor,
      "grade":this.secondFormGroup.value.grade,
      "subjects":this.secondFormGroup.value.subjects,
      "details":this.secondFormGroup.value.details,
      "modeofteaching":this.thirdFormGroup.value.modeofteaching,
      "gender":this.thirdFormGroup.value.gender,
      "budget":this.thirdFormGroup.value.budget,
      "budget1":this.thirdFormGroup.value.password,
      "document":this.thirdFormGroup.value.document

    }
    console.log(data1);
    localStorage.setItem('data1', JSON.stringify(data1));
    this.router.navigate(['/parentdashboard']);
  
  }
      // convenience getter for easy access to form fields
      get f() { return this.firstFormGroup.controls;
        return this.secondFormGroup.controls;
        return this.thirdFormGroup.controls; }

        onSubmit() {
          // if (this.formGroup.valid) {
          //   console.log("Form Submitted!");
          // }
          //
          // this.submitted = true;
      
          //     // stop here if form is invalid
          //     if (this.firstFormGroup.invalid) {
          //         return;
          //     }
      
          //  alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.formGroup.value))
        }

}
